package cellular;

import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;


public class BriansBrain implements CellAutomaton {
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				nextGeneration.set(i,j,getNextCell(i, j));
			}
		}
		currentGeneration = nextGeneration; 
    }

    @Override
    public CellState getNextCell(int row, int col) {
        if (getCellState(row, col)==CellState.ALIVE) {
			return CellState.DYING;}
		if (getCellState(row, col)==CellState.DYING) {
			return CellState.DEAD;}

		if (getCellState(row, col)==CellState.DEAD && countNeighbors(row, col, getCellState(row, col))== 2) {
				return CellState.ALIVE;}
		else return CellState.DEAD;
		
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    private int countNeighbors(int row, int col, CellState state) {
		int neighbour = 0;
		for (int i = row-1; i <= row+1; i++){
			for (int j = col-1; j <= col+1; j++){
				if (i >= currentGeneration.numRows() || j >= currentGeneration.numColumns() || i < 0 || j < 0) {
					continue;
				}
				if (i == row && j == col) {
					continue;
				}
				else if (currentGeneration.get(i,j) == CellState.ALIVE) {
					neighbour++;
				}
			}
		}
		return neighbour;
	}
}
