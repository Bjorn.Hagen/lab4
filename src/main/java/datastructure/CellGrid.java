package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        grid = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        return grid.length;
    }

    @Override
    public int numColumns() {
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column]=element;
    }

    @Override
    public CellState get(int row, int column) {
             return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid copy = new CellGrid(numRows(),numColumns(), CellState.DEAD);

        for(int i = 0; i < numRows();i++) {
            for (int j = 0; j < numColumns();j++) {
                copy.set(i, j, grid[i][j]);
            } 
        }


        return (IGrid) copy;
    }
    
}